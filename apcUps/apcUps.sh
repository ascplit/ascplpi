#!/bin/bash

# optionally disable this script if UPSDISABLE is defined in local config
if [[ -f "/opt/ascplPi/ascplPi.conf" ]] ; then
  source "/opt/ascplPi/ascplPi.conf"
  if [[ $UPSDISABLE ]] ; then
    exit 0
  fi
fi

# get the UPS status
UPSSTATUS=$(/sbin/apcaccess status | grep STATUS | awk '{print $3}')

# translate UPS status to Nagios check_mk status
if [[ $UPSSTATUS == "ONLINE" ]] ; then
  STATUS=0
  STATUSTXT=OK
elif [[ $UPSSTATUS == "COMMLOST" ]] ; then
  STATUS=1
  STATUSTXT=WARNING
elif [[ $UPSSTATUS == "ONBATT" ]] ; then
  STATUS=2
  STATUSTXT=CRITICAL
elif [[ $UPSSTATUS == "SHUTTING" ]] ; then
  UPSSTATUS=SHUTDOWN
  STATUS=2
  STATUSTXT=CRITICAL
fi

# return data to Nagios check_mk
echo "$STATUS UPS_Status UPSSTATUS=$UPSSTATUS;2;2;1;0; $STATUSTXT - $UPSSTATUS"
