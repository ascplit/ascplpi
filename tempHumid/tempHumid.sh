#!/bin/bash

# ascplPi.conf is optionally configured
if [[ -f "/opt/ascplPi/ascplPi.conf" ]] ; then
  source "/opt/ascplPi/ascplPi.conf"
fi

# C program extracts data from sensor, store in variable
(/opt/ascplPi/tempHumid/rht03.bin > /tmp/rht03.log &)
sleep 8
killall "rht03.bin"
READING=$(tail -n 2 /tmp/rht03.log)
rm /tmp/rht03.log

# Parse the data out, convert temp to F, and round up both
TEMP=$(echo $READING | awk '{print $5}' | sed s/','//)
HUMID=$(echo $READING | awk '{print $7}' | sed s/'%'//)
HUMID=$(perl -e "print (($HUMID == int($HUMID)) ? $HUMID : int($HUMID + 1))")
TEMP=$(perl -e "print $TEMP*9/5+32")
TEMP=$(perl -e "print (($TEMP == int($TEMP)) ? $TEMP : int($TEMP + 1))")

# TEMPMOD is optionally defined in ascplPi.conf
if [[ $TEMPMOD ]] ; then
  TEMP=$(perl -e "print ($TEMP + $TEMPMOD)")
fi

# Nagios stuff
if [[ $TEMP -lt 77 ]] ; then
  STATUS=0
  STATUSTXT=OK
elif [[ $TEMP -lt 81 ]] ; then
  STATUS=1
  STATUSTXT=WARNING
else
  STATUS=2
  STATUSTXT=CRITICAL
fi
echo "$STATUS Climate_Temperature TEMP=$TEMP;77;81;0; $STATUSTXT - $TEMP ° F"

if [[ $HUMID -lt 56 ]] ; then
  STATUS=0
  STATUSTXT=OK
elif [[ $HUMID -lt 61 ]] ; then
  STATUS=1
  STATUSTXT=WARNING
else
  STATUS=2
  STATUSTXT=CRITICAL
fi
echo "$STATUS Climate_Humidity HUMID=$HUMID;56;61;0; $STATUSTXT - $HUMID % humidity"

