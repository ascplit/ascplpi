#!/bin/bash

cd "$(dirname "${BASH_SOURCE}")"
source install.conf

install() {
  RUNNINGPATH=$(pwd -P)
  if [[ $RUNNINGPATH != "${INSTALLPATH}install" ]] ; then
    echo ascplPi installer is not running from the install directory
    if [[ ! -d ${INSTALLPATH} ]] ; then
      echo ascplPi not installed, installing
      cd ..
      mv $(pwd -P) ${INSTALLPATH}
    fi
    if [[ -d ${INSTALLPATH} ]] ; then
      cd "${INSTALLPATH}install"
      install
    else
      exit 1
    fi
  else
    echo ascplPi is installed
    echo verifying project package dependencies
    packages
    echo updating files and permissions
    files
    vncPass
    echo updating cron jobs
    cronJob
    echo done
  fi
}

packages() {
  apt-get update > /dev/null
  apt-get -y install apcupsd check-mk-agent conky git putty wifi-radar x11vnc xscreensaver  > /dev/null
}

files() {
  find ${INSTALLPATH} -name "*sh" -exec chmod 774 {} \;
  ln -s ${INSTALLPATH}apcUps/apcUps.sh /usr/lib/check_mk_agent/local/apcUps.sh
  pushd ${INSTALLPATH}tempHumid
  wget "${BBREP}ascplpi/downloads/rht03.bin"
  popd
  chmod 774 ${INSTALLPATH}tempHumid/rht03.bin ${INSTALLPATH}wollog/wollog ${INSTALLPATH}wollog/wollog.pl
  ln -s ${INSTALLPATH}tempHumid/tempHumid.sh /usr/lib/check_mk_agent/local/tempHumid.sh
  ln -s ${INSTALLPATH}wifiScan/wifiScan.sh /usr/lib/check_mk_agent/local/wifiScan.sh
  ln -s ${INSTALLPATH}wollog/wollogTrigger.sh /usr/lib/check_mk_agent/local/wollogTrigger.sh
  mkdir ${HOMEPATH}startup
  chown pi:pi ${HOMEPATH}startup
  ln -s ${INSTALLPATH}pi/startup.sh /home/pi/startup/startup.sh
  chmod +x ${INSTALLPATH}pi/startup.sh
  ln -s ${INSTALLPATH}wollog/wollog /etc/init.d/wollog
  cp ${INSTALLPATH}pi/conkyrc ${HOMEPATH}.conkyrc
  chown pi:pi ${HOMEPATH}.conkyrc
  mkdir ${HOMEPATH}.config/autostart
  cp ${INSTALLPATH}pi/startup.sh.desktop ${HOMEPATH}.config/autostart/startup.sh.desktop
  chown -R pi:pi ${HOMEPATH}.config/autostart
  cp ${INSTALLPATH}pi/xscreensaver ${HOMEPATH}.xscreensaver
  chown pi:pi ${HOMEPATH}.xscreensaver
  systemctl enable wollog
}

vncPass() {
  # only ask for password if run from terminal
  if [[ -t 1 ]] ; then
    read -s -t 10 -p "create VNC password =>" VNCPASS
    if [[ ${VNCPASS} ]] ; then
      mkdir /home/pi/.vnc
      /usr/bin/x11vnc -storepasswd ${VNCPASS} /home/pi/.vnc/passwd
      chown -R pi:pi /home/pi/.vnc
    else
      echo a VNC password will have to be created manually
    fi
  fi
}

cronJob() {
  crontab -l > /tmp/crontab_TEMP1
  grep -v "ascplPi" /tmp/crontab_TEMP1 > /tmp/crontab_TEMP2
  echo -e "59 23 1 * * ${INSTALLPATH}wollog/wollogTrim.sh" >> /tmp/crontab_TEMP2
  echo -e "1 * * * * ${INSTALLPATH}install/update.sh" >> /tmp/crontab_TEMP2
  cat /tmp/crontab_TEMP2 | crontab
  rm /tmp/crontab_TEMP*
}

install
