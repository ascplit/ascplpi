#!/bin/bash

cd "$(dirname "${BASH_SOURCE}")"
source install.conf

if [[ ! -d "${INSTALLPATH}.git" ]] ; then
  echo installation not originally cloned by git, initializing
  /usr/bin/git -C ${INSTALLPATH} init
  /usr/bin/git -C ${INSTALLPATH} remote add origin "${BBREP}ascplpi.git"
fi

/usr/bin/git -C ${INSTALLPATH} remote update
/usr/bin/git -C ${INSTALLPATH} clean -fd -e "ascplPi.conf" -e "rht03.bin"
/usr/bin/git -C ${INSTALLPATH} reset --hard

PULLSTATUS=$(git -C ${INSTALLPATH} pull origin master 2> /dev/null)
find ${INSTALLPATH} -name "*sh" -exec chmod 774 {} \;

if [[ ${PULLSTATUS} == *'Already up-to-date'* ]] ; then
  echo ascplPi is already up-to-date
else
  echo ascplPi has updated, installing
  ${INSTALLPATH}/install/install.sh
fi
