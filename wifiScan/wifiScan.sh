#!/bin/bash

# scan for relevant SSIDs and count them
WLANLIST=$(iwlist wlan0 scan | grep ESSID)
WLANCOUNT=0

if [[ $(echo $WLANLIST | grep Akron-Library) ]] ; then
  ((WLANCOUNT=$WLANCOUNT + 1))
fi

if [[ $(echo $WLANLIST | grep ASCPL-Co-Op) ]] ; then
  ((WLANCOUNT=$WLANCOUNT + 1))
fi

# return data to Nagios check_mk
if [[ $WLANCOUNT -eq 2  ]] ; then
  STATUS=0
  STATUSTXT=OK
else
  STATUS=2
  STATUSTXT=CRITICAL
fi

echo "$STATUS WiFi_SSIDs WLANCOUNT=$WLANCOUNT;2;0; $STATUSTXT - $WLANCOUNT"
