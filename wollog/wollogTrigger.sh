#!/bin/bash

LOGPACKET="/var/log/wollog.log"

# parse last two lines of wollog daemon log
LASTTWO=$(tail -n 2 $LOGPACKET | awk '{print $9}' | head -n 1)
LASTONE=$(tail -n 1 $LOGPACKET | awk '{print $9}')

# check if data is numeric and
# whether last two packets were received within 24 hours
NOW=$(date +%s)

if [[ $LASTTWO =~ ^[0-9]+$ ]] ; then
  ((LASTTWO=$NOW - $LASTTWO))
  if [[ $LASTTWO -lt 86400 ]] ; then
    LASTTWO=1
  else
    LASTTWO=0
  fi
else
  LASTTWO=0
fi

if [[ $LASTONE =~ ^[0-9]+$ ]] ; then
  ((LASTONE=$NOW - $LASTONE))
  if [[ $LASTONE -lt 86400 ]] ; then
    LASTONE=1
  else
    LASTONE=0
  fi
else
  LASTONE=0
fi

((LAST24=$LASTTWO + $LASTONE))

# return data to Nagios check_mk
if [[ $LAST24 -eq 2  ]] ; then
  STATUS=0
  STATUSTXT=OK
elif [[ $LAST24 -eq 1 ]] ; then
  STATUS=1
  STATUSTXT=WARNING
else
  STATUS=2
  STATUSTXT=CRITICAL
fi
echo "$STATUS WOL_Within_24h LAST24=$LAST24;2;1;0; $STATUSTXT - $LAST24"
