#!/bin/bash

LOGPACKET="/var/log/wollog.log"
LOGSTATUS="/var/log/wollogStatus.log"

#PACKETLEN=$(cat $LOGPACKET | wc -l)
#STATUSLEN=$(cat $LOGSTATUS | wc -l)

/etc/init.d/wollog stop
tail -n 1000 $LOGPACKET > ${LOGPACKET}_TEMP
mv ${LOGPACKET}_TEMP $LOGPACKET
tail -n 1000 $LOGSTATUS > ${LOGSTATUS}_TEMP
mv ${LOGSTATUS}_TEMP $LOGSTATUS
/etc/init.d/wollog start
