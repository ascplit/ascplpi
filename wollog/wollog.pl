#!/usr/bin/perl

use strict;
use warnings;
use IO::Socket::INET;

my $logPacket = "/var/log/wollog.log";
my $logStatus = "/var/log/wollogStatus.log";

# UID check
sub rootCheck {
  if ($< > 0) {
    return 0;
  } elsif ($< == 0) {
    return 1;
  } else {
    die "UID error";
  }
}

# process command flags
sub processArguments {
  my $error = "wollog takes exactly one of the following arguments\n"
  . "\t--start\tstarts wollog service\n"
  . "\t--stop\tstops wollog service\n"
  . "\t--status\tstatus of wollog service\n"
  . "\t--help\tshows this help text\n";

  if ($#ARGV > 0 || $#ARGV < 0) {
    die $error;
  }

  if ($ARGV[0] eq "--start") {
    startService();
  } elsif ($ARGV[0] eq "--stop") {
    stopService();
  } elsif ($ARGV[0] eq "--status") {
    statusService();
  } elsif ($ARGV[0] eq "--help") {
    print $error;
    exit 0;
  } else {
    die $error;
  }
}

# date/time stamp
sub dtStamp {
  my @dateTime = localtime(time);
  $dateTime[5] = $dateTime[5] + 1900;
  $dateTime[4] = $dateTime[4] + 1;
  for (my $i = 0; $i < 5; $i++) {
    $dateTime[$i] = sprintf("%02d", $dateTime[$i]);
  }
  my $stamp = "$dateTime[4]/$dateTime[3]/$dateTime[5] $dateTime[2]:$dateTime[1]:$dateTime[0]";
  return $stamp;
}

# logger
sub logIt {
  (my $logFile, my $logMessage) = @_;
  my $stamp = dtStamp();
  open(LOGFILE, ">>$logFile");
  flock(LOGFILE, 2);
  print LOGFILE "$stamp => $logMessage\n";
  close(LOGFILE);
}

# WOL magic packet listener
sub wolListen {
  # flush after every write
  $| = 1;
  
  my $error = "";

  # create UDP Socket and bind to LocalPort
  my $socket = IO::Socket::INET->new (
    LocalPort => '9',
    Proto => 'udp',
  ) or $error = "wollog service cannot start - cannot bind to port";

  if ($error ne "") {
    logIt($logStatus, $error);
    die("$error\n");
  }

  my $syncStream = "";

  # read from the socket
  $socket->recv(my $hexPacket, 256);

  # hex dump the received data and truncate it
  $syncStream = substr(unpack('h*', $hexPacket), 0, 12);

  $socket->close();

  return $syncStream;
}

sub startService {
  logIt($logStatus, "wollog service started");

  my $ss = "";
  
  while ($ss ne '000000000000' ) {
    $ss = wolListen();
    my $epoch = time();
    if ($ss eq 'ffffffffffff') {
      logIt($logPacket, "WOL packet received at epoch $epoch");
    } elsif ($ss eq '888888888888') {
      logIt($logStatus, "wollog service status queried at epoch $epoch");
    }
  }
  
  if ($ss eq '000000000000') {
    logIt($logStatus, "wollog service stopped cleanly");
  }
}

sub stopService {
  my $socket = new IO::Socket::INET (
    PeerAddr => '127.0.0.1',
    PeerPort => '9',
    Proto    => 'udp',
  );
  
  $socket->send(pack('h*', "000000000000"));
}

sub statusService {
  my $socket = new IO::Socket::INET (
    PeerAddr => '127.0.0.1',
    PeerPort => '9',
    Proto    => 'udp',
  );
  
  $socket->send(pack('h*', "888888888888"));
}

# procedure
rootCheck() or die "must be run as root";

processArguments();
